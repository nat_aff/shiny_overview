

# Shiny app basics


## Structure of an App

A shiny app consists of 

* the UI 
* the server

**Using separate files**
```R
library(shiny)

 shinyUI(fluidPage(fluidRow(column(3,...)
        )# row  
      )# page
    )# ui

  # the server.R file

library(shiny)

shinyServer(function(input, output) {

})

## Run the app

shinyApp(ui = ui, server = server)

```
The files must be named _ui.R, server.R_ 


**Using a single file**
```R
  ui <- fixedPage(fluidRow(column(3, ... )
    )# row
  )# page

 # in the same file
  
server <- function(input,output){ 
      # functions here
  }

shinyApp(ui = ui, server = server)
```

## Layout

Two main types are

* fixedPage : does not adjust to the browser window size
* fluidPage : adjusts to browser window size

* sidebarLayout : underneath it is just a grid layout 
* grid layout : more customizable

**sidebar Layout**

```R
shinyUI(fluidPage(

  titlePanel("Hello Shiny!"),

  sidebarLayout(

      sidebarPanel(
      ),

      mainPanel(
      )
    ) # layout
  )# page
)# ui


```

### Grid layout

The same layout as above with a grid. The column widths for a given row must
add up to 12. Columns can be subdivided into rows that hold additional columns.

For more info see: [bootstrap scaffodling](http://getbootstrap.com/2.3.2/scaffolding.html), (this is the system shiny is built with).

```R
shinyUI(fluidPage(

  titlePanel("Hello Shiny!"),

  fluidRow(
  
      column(4,
        wellPanel(
      ),

      column(8,
      )
        ) # layout
  )# page
)# ui

```
### Design layout separately**

You can simplify layout design by building up components of your layout in isoloated stages.

Example where the behavior of fluid rows and columns can be visualized.
Offset is used in the first column of the second row. 

```R
library(shiny)

ui <- fluidPage(
  fluidRow(
    column(12, style = "background-color:blue;", div(style = "height:100px;"))
    ),# row
        
    fluidRow( column(6, offset = 1, style = "background-color:yellow;", div(style = "height:300px;")),
              column(5, style = "background-color:green", div(style = "height:300px;"))
    ), # row   

  fluidRow(
       column(12, style="background-color:blue;", tags$div(style = "height:400px;"))
 ) # row
) # page
  


  server = function(input, output) {
  }


shinyApp(ui = ui, server = server)
```

## Text and numeric input and output

Shiny widgets all have the first two arguments in common **name** and a **label**. The name is used to access the input variable and the label is what appears in the layout.

[Here is a tutorial](http://shiny.rstudio.com/tutorial/lesson3/) overview of shiny widgets.

### Examples

**numericInput**
```R
numericInput("num1", 
        label = h3("Numeric input"), 
        value = 1))   
```


**Help text**
```R
column(3, 
      h3("Help text"),
      helpText("Note: help text isn't a true widget,", 
        "but it provides an easy way to add text to",
        "accompany other widgets.")),
```



## Shiny Output


**Output types**

To display output, shiny has a number of methods: 

* htmlOutput  raw HTML
* imageOutput image
* plotOutput  plot
* tableOutput table
* textOutput  text
* uiOutput  raw HTML
* verbatimTextOutput  text

The output takes a single argument, a string that is defined as output of one of the _render_ functions.

```R
textOutput("text1")
```

**Render types**

* renderImage images (saved as a link to a source file)
* renderPlot  plots
* renderPrint any printed output
* renderTable data frame, matrix, other table like structures
* renderText  character strings
* renderUI  a Shiny tag object or HTML

Render functions check for updated information and ouputs are updated when new data is detected. Here's a minimal example that updates some text.

```R
library(shiny)

ui <- fluidPage(
  titlePanel("Reactive output"),
  sidebarLayout(

      sidebarPanel(
        textInput("text1", h3("Reactive text input"))
      ),

      mainPanel(
        textOutput("rtext1")
      )
    
    ) # layout
  )# page



server <- function(input, output) {
  
    output$rtext1 <- renderText({ 
      paste("Hello ", input$text1)
    })
  }

shinyApp(ui, server)

```

**When functions run**

Using our previous example, here's when each part of the server function would run.
```R

# code in the server file but outside the server function
# runs once when the file is loaded

server <- function(input, output) {

  # code here runs once when the file is loaded
  
  output$rtext1 <- renderText({ 

    # code here runs every time the widget with the
    # "input$text1" variable is updated 

      paste("Hello ", input$text1)
  })
}

shinyApp(ui, server)

```


## Reactive Expressions


If you are using multiple values to updatean _rendered_ item, you can use reactive expressions to reduce the need to update calculations that haven't changed. 

From [this shiny tutorial](http://shiny.rstudio.com/tutorial/lesson6/)

**Reactive function basics**

* A reactive expression takes input values, or values from other reactive expressions, and returns a new value

* Reactive expressions save their results, and will only re-calculate if their input has changed

* Create reactive expressions with reactive({ })

* Call reactive expressions with the name of the expression followed by parentheses ()

* Only call reactive expressions from within other reactive expressions or render* functions

### Action button 

There are multiple ways to isolate a function so that rendered output only updates when select fields are updated. 

Here's an example using an action button that only updates output when the "compute" button is selected.

```R

library(shiny)

ui <- fluidPage(
  TitlePanel("Reactive output"),
  sidebarLayout(

      sidebarPanel(
        numericInput("n1", "input1", 0), 
        numericInput("n2", "input2", 0), 
        numericInput("n3", "input3", 0),
        actionButton("compute", "Sum inputs")
      ),

      mainPanel(wellPanel( textOutput("rtext")))
      ) # layout
  ) # page

server <- function(input, output) {
  
  # sumText() updates the sum when the comput button is pressed. 
  # The renderText function only re-runs when sumText() is updated.
  
  sumText <- eventReactive(input$compute, {
    
    if(input$n1 == 10) stopApp(1)
    return( input$n1 + input$n2 + input$n3)
  })
  
  output$rtext <- renderText({
    sumText()
  })
}

shinyApp(ui, server)

```
### Using multiple inputs from selectInput

When using a 'selectInput' widget, the return value associated with the selected input is a character. In the example below, `s1` is the input variable name. The input$s1 variable will return "A" if "30" is selected, "B" if "20" is selected, and so-forth. 



```R
 # in ui.R
 selectInput("s1", "select1",
                  choices = list("30" = "A", 
                           "20" = "B", 
                           "10" = "C",
                            "Exit" = "Exit" ), 
                              selected = 1),


```

In the server, a value can be retrieved using a switch statement. The switch statement checks which character value `input$s1` and returns the associated numerical value.


```R
val1 <- switch(input$s1, 
            "A" = 30,
            "B" = 20,
            "C" = 10)


```

The `multipleInputs.R` example has a working example of the selectInput widget.

### Outputing HTML

(coming next)